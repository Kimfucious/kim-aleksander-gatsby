---
path: "/blog/input-group-component-shared-across-forms"
date: "2018-06-18"
title: "Input Group Component Shared Across Forms"
---

I'm using Gatsby on a project, but this is really a question about React.

I've created a styled InputGroup component that I was hoping to share amongst various forms. I'm using some `CSS-in-JS` and `styled-system` to do the styling, but that doesn't have any bearing on my question.

The code for the component is at the bottom on this post.

In brief, the component renders an input field with an icon. The name the icon is passed as a prop so that different fields can have different icons. The field name, placeholder, and type are also passed as props.

This all seems to work nicely, when I do something like this:

```html
<form method="POST">
  <input
    icon="fas fa-user-circle"
    name="username"
    placeholder="Snidely Whiplash"
    type="text"
  />
  <input
    icon="fas fa-envelope"
    name="email"
    placeholder="example@example.com"
    type="email"
  />
  <button>Send</button>
</form>
```

However, when I try to do this:

```html
<Input icon="fas fa-user-circle" name="username"
onChange={this.handleInputChange} // <= call function on click to update state
placeholder="Joe Schmoe" type="text" value={this.state.username} // <= update
the value of this field using state />
```

The state is not getting updated. The full code for the conact form is below, but the handler function is pretty straightforwrad at this point. The `toUpperCase()` bit is there to test that `value={this.state.username}`, is putting the value from the state into the field.

## Observations

1.  If I change and of the Input fields to input, the state gets updated as expected, but I lose all of the styling. It's obvious why the styling is lost. I believe that the state is not getting updated, because
2.  The `TextArea` styled component **does** update the state. I'm not exactly sure why that's working, but I believe it's because that styled component exists as a global variable within the same scope as does the `Connect` class component.
3.  The `TextArea` fields does **not** get updated from `value={this.state.value}`. And I'm pretty sure that this is for the same reason that state is not getting updated on the Input fields: Input and TextArea don't have any idea what the state is in Connect.

To remedy Input not updating state, I passed down `onClick` as a prop to Input, as I do icon, name, placeholder, and type.

## Question

With the assumption that having nicely styled form fields is a good thing, and that styled components are a good way to do this, what's the best way to

```js
handleInputChange(event) {
  const target = event.target;
  const value = target.value;
  const name = target.name;

  this.setState({
    [name]: value.toUpperCase(),
  });
}
```

### Code for Contact Form (Connect)

```js
import React, { Component } from "react";
import styled from "@emotion/styled";
import Button from "./button";
import Flex from "./flex_column";
import Flex100 from "./flex_column_100vh";
import Input from "./input";
import Title from "./title";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";

const TextArea = styled.textarea`
  background-color: rgba(0, 0, 0, 0.6);
  border: none;
  border-radius: 0.35rem;
  color: white;
  font-family: "Exo 2", sans-serif;
  padding: 1rem;
  &:focus {
    outline: none;
  }
  &:invalid {
    box-shadow: none;
  }
`;

class Connect extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "", email: "", url: "", subject: "", message: "" };
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value.toUpperCase(),
    });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <Flex100 py={4}>
        <Title fontSize={[5, 6, 6, 7]} mb={[3, 4]}>
          Connect
        </Title>
        <form method="POST" name="connect" onSubmit={this.handleSubmit}>
          <Flex mb={"4em"}>
            <Flex width={[90 / 100]}>
              <Input
                icon="fas fa-user-circle"
                name="username"
                onChange={this.handleInputChange}
                placeholder="Dick Dasterdley"
                type="text"
                value={this.state.username}
              />
              <Input
                icon="fas fa-envelope"
                name="email"
                onChange={this.handleInputChange}
                placeholder="example@example.com"
                type="email"
                value={this.state.email}
              />
              <Input
                icon="fas fa-globe"
                name="url"
                placeholder="https://www.example.com"
                type="url"
                value={this.state.url}
              />
              <Input
                icon="fas fa-comment-alt"
                name="subject"
                onChange={this.handleInputChange}
                placeholder="Subject"
                type="text"
                value={this.state.subject}
              />
              <TextArea
                style={{ width: "100%" }}
                rows="10"
                cols="50"
                name="message"
                onChange={this.handleInputChange}
                placeholder="Your Message..."
                required
                value={this.state.value}
              />
              <br />
              <Button
                style={{ width: "100%" }}
                fontSize={[2, 2, 3]}
                my={3}
                p={".6em"}
              >
                Send
              </Button>
            </Flex>
          </Flex>
        </form>
      </Flex100>
    );
  }
}

export default Connect;
```

### Code for InputGroup Component

```js
import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "@emotion/styled";
import { space } from "styled-system";
import "../utils/fonts/fontawesome";
import "../utils/fonts/fa-solid";

const IconInput = styled.div`
  ${space};
  background-color: rgba(0, 0, 0, 0.6);
  border-radius: 0.35rem;
  font-family: "Exo 2", sans-serif;
  padding: 0.75rem 1rem;
`;

class InputGroup extends React.Component {
  render() {
    return (
      <IconInput
        mb={3}
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <span style={{}}>
          <i style={{ color: "#8e8e8e" }} className={this.props.icon} />
        </span>
        <input
          className={css`
            flex: 1;
            background: transparent;
            border: none;
            color: white;
            padding-left: 1rem;
            &:focus {
              outline: none;
            }
            &:invalid {
              border: none;
            }
            @media (min-width: 768px) {
              min-width: 250px;
            }
          `}
          name={this.props.name}
          placeholder={this.props.placeholder}
          required
          type="text"
        />
      </IconInput>
    );
  }
}

InputGroup.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
  placeholder: PropTypes.string,
};

export default InputGroup;
```
