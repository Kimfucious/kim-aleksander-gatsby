import React from "react";
import styled from "@emotion/styled";
import Anchor from "../components/anchor_button";
import Scrollchor from "../utils/scrollchor-item";
import Swiper from "react-id-swiper";
import Title from "../components/title";
import "swiper/dist/css/swiper.css";
import "typeface-sanchez";

const Container = styled.div`
  color: white;
  display: flex;
  flex-flow: column nowrap;
  font-family: "Sanchez", serif;
  height: 100vh;
  justify-content: center;
  text-align: center;
  text-shadow: 2px 2px black;
  padding: 0rem 1rem;
`;

const SwiperContainer = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 0.75rem;
  margin: 1rem 0rem 2rem 0rem;
  @media (min-width: 576px) {
    margin: 2rem 2rem 3rem 2rem;
    padding: 0rem 2rem;
  }
`;
class Reviews extends React.Component {
  render() {
    const params = {
      loop: true,
      slidesPerView: 3,
      spaceBetween: 30,
      speed: 600,
      pagination: { el: ".swiper-pagination", clickable: true },
      breakpoints: {
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        768: {
          slidesPerView: 2,
        },
        576: {
          slidesPerView: 1,
        },
      },
    };
    return (
      <div>
        <Container>
          <Title fontSize={[5, 6, 6, 7]} my={[0, 3]}>
            False Positives
          </Title>
          <h3 style={{ padding: "0rem 1rem" }}>
            A Washington D.C. based techno-thriller with action taking place in
            Bangkok, Teheran, and Saigon.
          </h3>
          <div>
            <SwiperContainer>
              <Swiper {...params}>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "A well-paced and imaginative page turner that will appeal
                    not only to techno-junkies but to anyone who likes a
                    well-spun yarn... Try it, you'll have fun."
                  </p>
                  <p>&mdash; The Kindle Book Review</p>
                </div>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "From the sweltering streets of Thailand to the corridors of
                    power in the U.S., the action builds to a frantic climax
                    that leaves the reader gasping."
                  </p>
                  <p>&mdash; Russell Blake Author of THE GERONIMO BREACH</p>
                </div>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "I can't say enough good things about this book. Once I
                    started reading it, I literally couldn't put it down."
                  </p>
                  <p>&mdash; GoodReads Review</p>
                </div>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "Kim Aleksander's "False Positives was a page turner. I
                    found it absolutely captivating..."
                  </p>
                  <p>&mdash; The TBR Pile</p>
                </div>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "Kim Alexander has created some memorable characters and
                    pretty much thrown the book at them. The story races along
                    to a thrilling and satisfying climax."
                  </p>
                  <p>&mdash; Amazon Reviewer</p>
                </div>
                <div
                  style={{
                    padding: "1em 1em",
                    textAlign: "left",
                  }}
                >
                  <p>
                    "Well thought out plot line and interesting characters.
                    Highly recommended. Waiting for the next one."
                  </p>
                  <p>
                    &mdash;{" "}
                    <a href="https://amzn.to/2l1Ln0c">Amazon Reviewer</a>
                  </p>
                </div>
              </Swiper>
            </SwiperContainer>
          </div>
          <div>
            <Anchor fontSize={[2, 2, 3]} mt={[1, 3]} px={[3, 3]} py={[2, 3]}>
              <Scrollchor to="#get-fp" animate={{ offset: 1, duration: 500 }}>
                get it now
              </Scrollchor>
            </Anchor>
          </div>
        </Container>
      </div>
    );
  }
}
export default Reviews;
