import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { css } from "emotion";
import { space } from "styled-system";
import "../utils/fonts/fontawesome";
import "../utils/fonts/fa-solid";

const IconInput = styled.div`
  ${space};
  background-color: rgba(0, 0, 0, 0.6);
  border-radius: 0.35rem;
  font-family: "Exo 2", sans-serif;
  padding: 0.75rem 1rem;
`;

class InputGroup extends React.Component {
  render() {
    return (
      <IconInput
        mb={3}
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <span style={{}}>
          <i style={{ color: "#8e8e8e" }} className={this.props.icon} />
        </span>
        <input
          className={css`
            flex: 1;
            background: transparent;
            border: none;
            color: white;
            padding-left: 1rem;
            &:focus {
              outline: none;
            }
            &:invalid {
              box-shadow: none;
            }
            @media (min-width: 768px) {
              min-width: 250px;
            }
          `}
          name={this.props.name}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}
          required
          type="text"
          value={this.props.value}
        />
      </IconInput>
    );
  }
}

InputGroup.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
};

export default InputGroup;
