import styled from "@emotion/styled";
import bgSmall from "../images/space_surfer-sm-1x.jpg";
import bgSmallWebp from "../images/space_surfer-sm-1x.webp";
import bgSmallRetina from "../images/space_surfer-sm-2x.jpg";
import bgSmallRetinaWebp from "../images/space_surfer-sm-2x.webp";
import bgMedium from "../images/space_surfer-md-1x.jpg";
import bgMediumWebp from "../images/space_surfer-md-1x.webp";
import bgMediumRetina from "../images/space_surfer-md-2x.jpg";
import bgMediumRetinaWebp from "../images/space_surfer-md-2x.webp";
import bgLarge from "../images/space_surfer-lg-1x.jpg";
import bgLargeWebp from "../images/space_surfer-lg-1x.webp";
import bgLargeRetina from "../images/space_surfer-lg-2x.jpg";
import bgLargeRetinaWebp from "../images/space_surfer-lg-2x.webp";
import bgXLarge from "../images/space_surfer-xl-1x.jpg";
import bgXLargeWebp from "../images/space_surfer-xl-1x.webp";
import bgXLargeRetina from "../images/space_surfer-xl-2x.jpg";
import bgXLargeRetinaWebp from "../images/space_surfer-xl-2x.webp";

export default styled.div`
  background-position: left bottom;
  background-repeat: no-repeat;
  background-size: cover;
  min-height: 100vh;
  position: relative;

  background-image: url("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QQmRXhpZgAASUkqAAgAAAACADIBAgAUAAAAJgAAAGmHBAABAAAAOgAAAEAAAAAyMDE4OjA2OjE4IDE5OjIwOjMzAAAAAAAAAAMAAwEEAAEAAAAGAAAAAQIEAAEAAABqAAAAAgIEAAEAAAC0AwAAAAAAAP/Y/+AAEEpGSUYAAQEAAAEAAQAA/9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8AAEQgAFQAoAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+ebfS7y6vBbW9vLJMTgIq5JNdBJ4D1eCy+0XIghycLGZAWPPfHQcdSag8K3N5BIhBJhYg7iTw3Zf/rV293rSLoC2ZE/mby+49EGeAK6adP2kbxRvCNKMOad27nN2Gm6dpEiPcSefMBnc0e5VPsM8/U1sxah4dukmS/sNRuMLxcRsi7enauVu/PndjsJXrzXo/hBdAl8H3NjqlpFFqUiEx3BYblPI4ya4sRKUVyyR6mGkpaU7JHEXXhGy1O2eWwvEGxDIwlAQhQMk8nBxRVDxMZbYSWsIdkHylgpAf86KmEbLRnNiKtNTtyJsxrS6ZWjkKqTE25c561oSavdshR5PMQnJDjNFFdtCcoXcXY8+T0sIuqyquFihBHfbUh168KhV2LjoQuKKKmVSUnqzSMnFaMqz3s1wxeZy7nqxooopIHJvVn//2f/bAEMAEAsMDgwKEA4NDhIREBMYKBoYFhYYMSMlHSg6Mz08OTM4N0BIXE5ARFdFNzhQbVFXX2JnaGc+TXF5cGR4XGVnY//bAEMBERISGBUYLxoaL2NCOEJjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY//CABEIABUAKAMBIgACEQEDEQH/xAAZAAEAAwEBAAAAAAAAAAAAAAAAAgMEAQX/xAAXAQEBAQEAAAAAAAAAAAAAAAACAAED/9oADAMBAAIQAxAAAAHxro61tFufaOmVBhqmMOlsRX//xAAcEAACAgMBAQAAAAAAAAAAAAAAAQISAxETISL/2gAIAQEAAQUCq2+MtKKibgx41JQbHL4Zjpzn4JlmWLs2f//EABgRAQEAAwAAAAAAAAAAAAAAAAEAICFh/9oACAEDAQE/AdRyUw//xAAXEQADAQAAAAAAAAAAAAAAAAAAAREQ/9oACAECAQE/AZR4i5//xAAaEAACAwEBAAAAAAAAAAAAAAABEQAQIQIg/9oACAEBAAY/AlHWgzDSojobF6//xAAdEAADAAEFAQAAAAAAAAAAAAAAARFRECExYXGR/9oACAEBAAE/IVAJtkaRdUZ7uvwV1/aKz7CnhlCmUVE/DL2DgeB5qVhaDZ8n/9oADAMBAAIAAwAAABAkUP8ABA//xAAXEQEBAQEAAAAAAAAAAAAAAAABABEx/9oACAEDAQE/EABrMZs5MKctv//EABkRAQEAAwEAAAAAAAAAAAAAAAEAECExEf/aAAgBAgEBPxA8NTTTBNOSnuP/xAAeEAEAAgMAAgMAAAAAAAAAAAABABEhMVFBYXGB4f/aAAgBAQABPxC6sYoI1bzyP5Cj7psHxBhg9wVFAcCuiEbM2vL3kEO+7vnqW3DUJRLbkmQCmlrcQaBtZElNHvMCKA+phAorhH1SvWf/2Q==");

  @media (min-width: 0px) {
    background-image: url(${bgSmall});
    background-image: -webkit-image-set(
      url(${bgSmallWebp}) 1x,
      url(${bgSmallRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgSmall}) 1x, url(${bgSmallRetina}) 2x);
  }

  @media (min-width: 576px) {
    background-image: url(${bgMedium});
    background-image: -webkit-image-set(
      url(${bgMediumWebp}) 1x,
      url(${bgMediumRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgMedium}) 1x, url(${bgMediumRetina}) 2x);
  }

  @media (min-width: 1024px) {
    background-image: url(${bgLarge});
    background-image: -webkit-image-set(
      url(${bgLargeWebp}) 1x,
      url(${bgLargeRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgLarge}) 1x, url(${bgLargeRetina}) 2x);
  }

  @media (min-width: 1440px) {
    background-position: center;
    background-image: url(${bgXLarge});
    background-image: -webkit-image-set(
      url(${bgXLargeWebp}) 1x,
      url(${bgXLargeRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgXLarge}) 1x, url(${bgXLargeRetina}) 2x);
  }
`;
