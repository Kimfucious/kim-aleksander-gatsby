import React from "react";
import styled from "@emotion/styled";
import AnchorButton from "./anchor_button";
import Title from "../components/title";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";

const Blurb = styled.div`
  align-items: center;
  display: flex;
  flex-flow: row nowrap;
  grid-area: blurb;
`;

const BuyButtons = styled.div`
  display: flex;
  flex-flow: column nowrap;
  grid-area: gn;
  justify-content: flex-start;
  text-align: center;
`;

const Section = styled("div")({
  color: "white",
  gridGap: "0rem 1.5rem",
  display: "grid",
  gridTemplateColumns: "1fr",
  gridTemplateAreas: `
    "title"
    "blurb"
    "gn"
  `,
  width: "90%",
  "@media(min-width: 768px)": {
    gridTemplateColumns: "2fr 1fr",
    gridTemplateAreas: `
      "title title"
      "blurb gn"
    `,
    width: "80%",
  },
});

class GetFalsePositives extends React.Component {
  render() {
    return (
      <Section>
        <Title fontSize={[5, 6, 6, 7]} mb={[3, 4]}>
          false positives
        </Title>
        <Blurb>
          <p
            style={{
              fontFamily: "Sanchez, serif",
              fontWeight: "100",
              backgroundColor: "rgba(0,0,0,0.6)",
              borderRadius: "0.75rem",
              padding: "2rem",
              textAlign: "justify",
              hyphens: "manual",
            }}
          >
            In 1972, a gifted student at Berkeley writes the first computer
            virus. When it’s run on the university mainframe it simply vanishes.
            Thirty-five years later, a government computer system issues
            ostensibly baseless assassination orders, and its creator goes in
            search of the ghost in the machine. What she discovers is a legacy
            black-ops program from the Vietnam Era that is alive and killing
            today. As she fights to prevent her brainchild from becoming a
            weapon for government-sanctioned murder, the protagonist is pitted
            against adversaries hell-bent on wielding the machine with
            Machiavellian ruthlessness to achieve their political ambitions.
            Joined by an eclectic band of characters, she plots to bring down
            the system before it is used to start a war of biblical proportions,
            and in doing so, she becomes marked for termination by her own
            creation.
          </p>
        </Blurb>
        <BuyButtons>
          <p
            style={{
              fontFamily: "'Exo 2', sans-serif",
              fontSize: "1.25rem",
              textShadow: "2px 2px black",
            }}
          >
            GET FALSE POSITIVES
          </p>
          <AnchorButton
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.amazon.com/FALSE-POSITIVES-Kim-Aleksander-ebook/dp/B006QVA8MW"
            fontSize={2}
            mt={0}
            mb={3}
            px={3}
            py={2}
          >
            Amazon
          </AnchorButton>
          <AnchorButton
            target="_blank"
            rel="noopener noreferrer"
            href="http://www.barnesandnoble.com/w/false-positives-kim-aleksander/1111014074"
            fontSize={2}
            mt={0}
            mb={3}
            px={3}
            py={2}
          >
            Barnes & Noble
          </AnchorButton>
          <AnchorButton
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.goodreads.com/book/show/13495786-false-positives"
            fontSize={2}
            mt={0}
            mb={3}
            px={3}
            py={2}
          >
            GoodReads
          </AnchorButton>
          <AnchorButton
            target="_blank"
            rel="noopener noreferrer"
            href="https://itunes.apple.com/us/book/false-positives/id516206471?mt=11"
            fontSize={2}
            mt={0}
            mb={3}
            px={3}
            py={2}
          >
            iBooks
          </AnchorButton>
          <AnchorButton
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.kobo.com/th/en/ebook/false-positives"
            fontSize={2}
            mt={0}
            mb={3}
            px={3}
            py={2}
          >
            Kobo
          </AnchorButton>
        </BuyButtons>
      </Section>
    );
  }
}
export default GetFalsePositives;
